/* Andreas Pack for Digitale Techniken, 2021-12
 *  Weather station incl.
 *    brightness
 *    air temperature
 *    humidity
 *    rtc clock
 *    CO2 sensor (type MHZ19B): Messwerterfassung durch PWM-Signal, Quelle: https://www.blikk.it
 *  logging to sd card with nicely formatted data output and creation of new files for each measurement
 *  incl. update after student reported bug (2021-21-23)
 *  incl. commented rtc.adjust (2022-01-13)
*/

#include "RTClib.h"; // For clock module
#include "Wire.h"; // For communication with clock & T,h sensor via I2C communication
#include "Adafruit_Si7021.h"; // For the T and h sensor
Adafruit_Si7021 sensor = Adafruit_Si7021();
#include "SPI.h"; // Library for SPI bus
#include "SD.h"; // Library for SD card module

RTC_DS1307 rtc;

int year;
int month;
int day;
int hour;
int minute;
int second;
int lightIntensity;
float relativeHumidity;
float temperature;
int ppmCO2;
String filename;

const int chipSelect = 10; // Variable as read only, when declared as constant

// CO2-Messung mit Sensor Typ MHZ19B
// Messwerterfassung durch PWM-Signal
// Quelle: https://www.blikk.it
// Implement this code into your code and write the CO2 concentration into the file on the SD card

// The sensor signal (pulse width modulation) is coming into PIN 7
const int pwmpin = 7;
// The measurement range is (0-5000 ppm CO2)
const int range = 5000;

int photozellePin = 0;

// Setup
void setup() {
  // Set PIN7 on input
  pinMode(pwmpin, INPUT);
  // Start serial communication
  Serial.begin(9600);
  // Clear console screen
  Serial.println("");
  Serial.println("*****************************");
  Serial.println("* Weather station           *");
  Serial.println("*****************************");
  while (!Serial) {
    // Waits for the serial port to be ready
  }
  if ( !SD.begin(chipSelect) )
  {
    Serial.println("-> SD card missing or defect.");
    return;
  }
  Serial.println("-> SD card is now correctly initialized.");
  // Start clock
  rtc.begin();

/* set the RTC to the date and time of compilation on your laptop with rtc.adjust
but only once (when connected to laptop via serial port)!
you then have to comment this line and recompile
else, everytime the weatherstation is connected to power and starts a measurement, the RTC will be adjusted to the time of compilation
*/
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    
  // Start T and h sensor module
  sensor.begin();
  
  // Create the filename
  DateTime now = rtc.now();
  year = now.year();
  month = now.month();
  day = now.day();
  hour = now.hour();
  minute = now.minute();
  second = now.second();
  if ( month < 10 )
  {
    filename += "0";
  }
  filename += month;
  if ( day < 10 )
  {
    filename += "0";
  }
  filename += day;
  if ( hour < 10 )
  {
    filename += "0";
  }
  filename += hour;
  if ( minute < 10 )
  {
    filename += "0";
  }
  filename += minute;
  filename += ".txt";
  // filename = "hallo.txt";
  // Now write the header
  File datafile = SD.open(filename, FILE_WRITE);
  // If file does not exist, a new file with the given filename is created
  if (datafile) // Check if the file exists
  {
    Serial.print("-> A file with filename ");
    Serial.print(filename);
    Serial.println(" has been created.");
    // Write the first line (header) into the text file
    datafile.println("dateTime,lightIntensity,temperature,relativeHumidity,ppmCO2");
    Serial.println("-> A header line has been written into the file.");
    Serial.println("");
  }
  datafile.close();
}

// The main program
void loop() {
  lightIntensity = analogRead( photozellePin ); // Get the signal from the phototransistor
  temperature = sensor.readTemperature();
  relativeHumidity = sensor.readHumidity();
  // Measure the CO2 concentration width with the function shown below the loop function
  ppmCO2 = readCO2PWM();
  DateTime now = rtc.now();
  year = now.year();
  month = now.month();
  day = now.day();
  hour = now.hour();
  minute = now.minute();
  second = now.second();

  // Print date and time to serial port
  Serial.print("-> ");
  Serial.print(year, DEC);
  Serial.print("-");
  if ( month < 10 )
  {
    Serial.print("0");
  }
  Serial.print(month, DEC);
  Serial.print("-");
  if ( day < 10 )
  {
    Serial.print("0");
  }
  Serial.print(day, DEC);
  Serial.print(" ");
  if ( hour < 10 )
  {
    Serial.print("0");
  }
  Serial.print(hour, DEC);
  Serial.print(":");
  if ( minute < 10 )
  {
    Serial.print("0");
  }
  Serial.print(minute, DEC);
  Serial.print(":");
  if ( second < 10 )
  {
    Serial.print("0");
  }
  Serial.print(second, DEC);
  Serial.print(",");
  Serial.print( lightIntensity );
  Serial.print(",");
  Serial.print( temperature, 2 );
  Serial.print(",");
  Serial.print( relativeHumidity, 2 );
  Serial.print(",");
  Serial.print(ppmCO2);

  // Now write things onto the SD card
  File datafile = SD.open(filename, FILE_WRITE);
  if (datafile) // Check if the file exists
  {
    Serial.println(", written to file.");
    datafile.print(year, DEC);
    datafile.print("-");
    if ( month < 10 )
    {
      datafile.print("0");
    }
    datafile.print(month, DEC);
    datafile.print("-");
    if ( day < 10 )
    {
      datafile.print("0");
    }
    datafile.print(day, DEC);
    datafile.print(" ");
    if ( hour < 10 )
    {
      datafile.print("0");
    }
    datafile.print(hour, DEC);
    datafile.print(":");
    if ( minute < 10 )
    {
      datafile.print("0");
    }
    datafile.print(minute, DEC);
    datafile.print(":");
    if ( second < 10 )
    {
      datafile.print("0");
    }
    datafile.print(second, DEC);
    datafile.print(",");
    datafile.print( lightIntensity );
    datafile.print(",");
    datafile.print( temperature, 2 );
    datafile.print(",");
    datafile.print( relativeHumidity, 2 );
    datafile.print(",");
    datafile.println(ppmCO2);
  }
  datafile.close();
  delay(100);
}


// This function reads the PWM signal width on PIN7
int readCO2PWM() {
  unsigned long th; // This is the width of the PWM signal in microseconds, maximum length is 1004 ms = 1004000 us
  int ppm_pwm = 0;
  float pulsepercent;
  // Wait for the measurement value
  do {
    th = pulseIn(pwmpin, HIGH, 2500000) / 1000; // Gives the witdh in milliseconds (division by 1000)
    // Pulse length in %
    float pulsepercent = th / 1004.0;
    // CO2 concentration in ppm
    ppm_pwm = range * pulsepercent;
  } while (th == 0);
  // Return the measured value to the main loop
  return ppm_pwm;
}
